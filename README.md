# Software Studio 2019 Spring Assignment 2
## Please use Chrome to play
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Bonus Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Bullet automatic aiming|5%|Y|
|Unique bullet|5%|Y|
|Helper|5%|Y|
|Boss|5%|Y|

## Website Detail Description
基本操作：
* ＷＡＳＤ控制飛行船方向
* 空白鍵發射子彈
* Ｆ 使用技能 
* p 暫停遊戲
* ＋調高音量、–調低音量
* HP代表血量，SP代表必殺技
* 吃到Ａ會射出自動追蹤的子彈，吃到Ｂ會射出特殊子彈，吃到Ｈ會有小幫手幫忙擋子彈，吃到Ｓ必殺技會加一
# Basic Components Description : 
1. Jucify mechanisms : 
* level : 我的level是會隨著遊戲進行而漸增，並沒有明確的level1、level2。一開始會有一隻怪，然後慢慢變兩隻、三隻、四隻，之後打完會有Boss。

* skill : 必殺技的話是射出一條激光，算持續傷害。很猛。
2. Animations :
* player的animations主要是讓飛船後方推進器會噴火，然後左右移動時機身會傾斜。

3. Particle Systems : 
* 子彈打到的時候會爆出不同的粒子。
4. Sound effects : 
* 背景音樂。
* 發射時會有音效。
5. Leaderboard : 進入遊戲前會要求輸入名字，然後會存到firebase，結束後會把最高的前十名讀出來。

# Bonus Functions Description : 
* 以下的觸發機制皆為在角色碰到補給包後，才能使用。
1.  Bullet automatic aiming : 吃到Ａ時，會先緩緩的射出五發子彈，然後過幾秒後再射向目標。（漢默丁格）

2. Unique bullet : 吃到Ｂ時，子彈會向ＤＮＡ一樣，雙股螺旋狀飛行，不會轉彎。

3. Helper : 吃到Ｈ時，會跑出一個嬰兒像衛星一樣的繞在飛機旁，碰到子彈時可以擋子彈。

4. super power : 吃到時，可以增加一個必殺技。

* Boss：會自動追蹤角色的位置，保持一定距離。子彈射出來的時候會是散射。蠻猛的。

* 操作說明：我在開始前另外加了操作說明，只要點選control就會跳出來，Hen貼心＾＾。
