
var loadState = {
	preload: function () {
		// Add a 'loading...' label on the screen
		var loadingLabel = game.add.text(game.width / 2, 150,
			'loading...', {
				font: '30px Arial',
				fill: '#ffffff'
			});
		loadingLabel.anchor.setTo(0.5, 0.5);
		// Display the progress bar
		var progressBar = game.add.sprite(game.width / 2, 200, 'progressBar');
		progressBar.anchor.setTo(0.5, 0.5);
		game.load.setPreloadSprite(progressBar);
		// Load all game assets
		game.load.spritesheet('player', 'assets/img/player1.png', 38, 45, 6);
		game.load.spritesheet('laser', 'assets/img/laserBeams.png', 56, 584, 4);
		game.load.spritesheet('button', 'assets/img/button.png', 189, 48);
		game.load.spritesheet('button2', 'assets/img/button2.png', 189, 48);
		game.load.image('blt_02', 'assets/img/bullet_02.png');
		game.load.image('blt_01', 'assets/img/bullet_01.png');
		game.load.image('blt_04', 'assets/img/bullet_04.png');
		game.load.image('e_blt_02', 'assets/img/e_bullet_02.png');
		game.load.image('enemy', 'assets/img/enemy_01.png');
		game.load.image('boss', 'assets/img/boss_01.png');
		game.load.image('Pause', 'assets/img/pause.png');
		game.load.image('pixel', 'assets/img/pixel.png');
		game.load.image('helper', 'assets/img/helper.png');
		game.load.image('A', 'assets/img/A.png');
		game.load.image('H', 'assets/img/H.png');
		game.load.image('S', 'assets/img/S.png');
		game.load.image('B', 'assets/img/B.png');
		game.load.image('ctrl', 'assets/img/control.png');
		game.load.image('volume', 'assets/img/volume.png');
//		game.load.image('explosion', 'assets/img/explosion.png');
		// Load a new asset that we will use in the menu state
		game.load.image('background', 'assets/img/background.jpg');
		game.load.image('cover', 'assets/img/cover.jpg');
		game.load.audio('bgm', ['assets/sound/bgm.mp3']);
		game.load.audio('pew_01', ['assets/sound/pew_01.ogg']);

	},
	create: function () {
		game.state.start('menu');
	}
};


