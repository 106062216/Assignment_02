var endState = {
	create: function () {
		var usrRef = firebase.database().ref('/users/');
		var obj;
		var score = [];
		game.add.image(0, 0, 'cover');

		usrRef.once('value').then(async function (snapshot) {
			obj = await snapshot.val();
			for (var key in obj) {
				score.push(obj[key]);
			}

			score.sort(function (a, b) {
				return b.score - a.score;
			});
			console.log(score);

			for (var i = 0; i < 10; i++) {
				if (score[i]) {
					var tmp = game.add.text(game.width / 2 - 100, game.height * 1 / 3 + 30 + 35 * i,
						(i + 1) + ".  " + score[i].name + ": " + score[i].score, {
							font: '25px Arial',
							fill: '#ffffff'
						});
					tmp.anchor.setTo(0, 0.5);
				}
			}

		});
		// Display the name of the game
		var scoreLabel = game.add.text(game.width / 2, game.height * 1 / 5,
			'Scoreboard', {
				font: '65px Arial',
				fill: '#ffffff'
			});
		scoreLabel.anchor.setTo(0.5, 0.5);

		var usrpt = game.add.text(game.width / 2, game.height * 1 / 5 + 60,
			 'You: ' + game.global.score, {
				font: '25px Arial',
				fill: '#ffffff'
			});
		usrpt.anchor.setTo(0.5, 0.5);



		var startLabel = game.add.text(game.width / 2, game.height,
			'press spacebar to continue', {
				font: '25px Arial',
				fill: '#ffffff'
			});
		startLabel.anchor.setTo(0.5, 1);

		var start = game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
		start.onDown.add(this.backToMenu, this);
	},
	backToMenu: function () {
		//		game.state.start('menu');
		window.location.href = "index.html";
	}
};
