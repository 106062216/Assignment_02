	var weapon;
	var enemies;
	var e_weapon = [];
	const e_num = 21;
	var set = [1, 1, 2, 2, 3, 3, 4, 4, 0];
	var e_index = 0;
	var e_dead = 0;
	var player;
	const speed = 200;
	var health;
	var power;
	var pew_01;
	var volumeLabel;
	var healthLabel;
	var powerLabel;
	var scoreLabel;
	var worldVo;
	var pause = false;
	var psImg;
	var emitter;
	var laserbeam;
	var boss;
	const laserDamage = 10;
	const bulletDamage = 1;
	var playerCount = 1;
	var t_start;
	var t_end;
	var blt_01;
	var blt_04;
	var aimBlt = [];
	var aimBlt_flag = 0;
	var baby;
	var babyHP = 0;
	var A;
	var H;
	var S;
	var B;
	var temp_time;
	var temp_x;
	var blt_04_flag = 0;
	var HP = [];
	var SP = [];
	var volume;

	var playState = {
		preload: function () {},
		create: function () {

			game.global.score = 0;
			this.background = game.add.tileSprite(0, 0, game.width, game.height, 'background');

			health = 3;
			//			healthLabel = game.add.text(10, 5, "HP: "
			//				, {
			//					font: '25px Arial',
			//					fill: '#ffffff'
			//				});
			//			healthLabel.anchor.setTo(0, 0);

			var temp_p = game.add.sprite(10, 5, 'player');
			temp_p.anchor.set(0, 0);
			HP.push(temp_p);
			temp_p = game.add.sprite(10 + temp_p.width + 10, 5, 'player');
			temp_p.anchor.set(0, 0);
			HP.push(temp_p);
			temp_p = game.add.sprite(10 + 2 * temp_p.width + 20, 5, 'player');
			temp_p.anchor.set(0, 0);
			HP.push(temp_p);

			power = 1;
			//			powerLabel = game.add.text(12, 5 + temp_p.height, "SP: " +
			//				power, {
			//					font: '25px Arial',
			//					fill: '#ffffff'
			//				});
			//			powerLabel.anchor.setTo(0, 0);

			var temp_S = game.add.sprite(10, 5 + 50, 'S');
			temp_S.anchor.set(0, 0);
			temp_S.scale.setTo(0.5, 0.5);
			SP.push(temp_S);

			scoreLabel = game.add.text(game.width / 2, 5,
				'score: ' + game.global.score, {
					font: '25px Arial',
					fill: '#ffffff'
				});
			scoreLabel.anchor.setTo(0.5, 0);



			//			H = game.add.sprite(game.width / 2, game.height / 2, 'H');
			//			H.anchor.set(0.5, 0.5);

			player = game.add.sprite(game.width / 2, game.height - 100, 'player');
			player.anchor.set(0.5, 0.5);
			game.physics.arcade.enable(player);
			player.body.collideWorldBounds = true;
			player.animations.add('flyIdle', [0, 1], 8, true);
			player.animations.add('flyRight', [2, 3], 8, true);
			player.animations.add('flyLeft', [4, 5], 8, true);
			console.log(player);

			baby = game.add.sprite(-200, -200, 'helper');
			baby.anchor.set(0.5, 0.5);
			baby.pivot.x = 300;
			baby.scale.setTo(0.25, 0.25);
			baby.visible = false;
			game.physics.arcade.enable(baby);


			blt_01 = game.add.group();
			blt_01.enableBody = true;
			blt_01.physicsBodyType = Phaser.Physics.ARCADE;
			blt_01.createMultiple(30, 'blt_01');
			blt_01.setAll('outOfBoundsKill', true);
			blt_01.setAll('checkWorldBounds', true);

			blt_04 = game.add.group();
			blt_04.enableBody = true;
			blt_04.physicsBodyType = Phaser.Physics.ARCADE;
			blt_04.createMultiple(30, 'blt_04');
			blt_04.setAll('outOfBoundsKill', true);
			blt_04.setAll('checkWorldBounds', true);

			this.blt_02 = game.add.weapon(3, 'blt_02');
			this.blt_02.enableBody = true;
			this.blt_02.physicsBodyType = Phaser.Physics.ARCADE;
			this.blt_02.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
			this.blt_02.fireRate = 100;
			this.blt_02.bulletSpeed = 600;
			this.blt_02.bulletAngleOffset = 90;
			this.blt_02.trackSprite(player, 0, -player.height / 2);

			laserbeam = game.add.sprite(0, 0, 'laser');
			laserbeam.anchor.set(0.5, 1);
			laserbeam.scale.x = 0.75;
			laserbeam.height = 0;
			game.physics.arcade.enable(laserbeam);
			laserbeam.animations.add('shoot', [0, 1, 2, 3], 8, true);
			//			laserbeam.visible = false;
			console.log(laserbeam);

			//			E = game.input.keyboard.addKey(Phaser.KeyCode.E);
			superPower = game.input.keyboard.addKey(Phaser.KeyCode.F);
			fireButton = game.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
			upKey = game.input.keyboard.addKey(Phaser.Keyboard.W);
			downKey = game.input.keyboard.addKey(Phaser.Keyboard.S);
			leftKey = game.input.keyboard.addKey(Phaser.Keyboard.A);
			rightKey = game.input.keyboard.addKey(Phaser.Keyboard.D);


			enemies = game.add.group();
			enemies.enableBody = true;
			enemies.physicsBodyType = Phaser.Physics.ARCADE;
			enemies.createMultiple(e_num - 1, 'enemy');
			enemies.createMultiple(1, 'boss');
			enemies.setAll('body.collideWorldBounds', true);
			enemies.setAll('body.bounce.x', 1);
			enemies.setAll('body.bounce.y', 1);

			console.log(enemies)
			//			game.time.events.loop(2000, this.addEnemy, this);

			for (var i = 0; i < e_num; i++) {
				if (i == e_num - 1) {
					var e_w = game.add.weapon(30, 'e_blt_02');
					e_w.enableBody = true;
					e_w.physicsBodyType = Phaser.Physics.ARCADE;
					e_w.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
					e_w.fireRate = 50;
					e_w.bulletSpeed = 400;
					e_w.bulletAngleVariance = 10;
					e_w.trackSprite(enemies.children[i], enemies.children[i].width / 2, 0, true);
					e_weapon.push(e_w);
				} else {
					var e_w = game.add.weapon(1, 'e_blt_02');
					e_w.enableBody = true;
					e_w.physicsBodyType = Phaser.Physics.ARCADE;
					e_w.bulletKillType = Phaser.Weapon.KILL_WORLD_BOUNDS;
					e_w.fireRate = 50;
					e_w.bulletSpeed = 400;
					e_w.trackSprite(enemies.children[i], enemies.children[i].width / 2, 0, true);
					e_weapon.push(e_w);
				}
			}
			console.log(e_weapon);

			game.time.events.add(2000, function () {
				this.addEnemy();
			}, this);

			setInterval(function () {
				x = 200 * Math.random() - 100;
				//				y = 200 * Math.random() - 100;
			}, 500);

			worldVol = 0.5;
			pew_01 = game.add.audio('pew_01');
			pew_01.volume = worldVol;

			bgm.restart();
			bgm.loop = true;
			bgm.volume = worldVol;
			//			volumeLabel = game.add.text(game.width - 10, 5, "Volume: " +
			//				bgm.volume, {
			//					font: '25px Arial',
			//					fill: '#ffffff'
			//				});
			//			volumeLabel.anchor.setTo(1, 0);
			volume = game.add.sprite(game.width - 185, 25, 'volume');
			volume.anchor.setTo(0, 0.5);
			volume.width *= bgm.volume;
			volume.scale.setTo(bgm.volume, bgm.volume);



			addVol = game.input.keyboard.addKey(187);
			subVol = game.input.keyboard.addKey(189);

			pauseBtn = game.input.keyboard.addKey(Phaser.Keyboard.P);
			pauseBtn.onDown.add(this.stop, self);
			psImg = game.add.image(game.width / 2, game.height / 2, 'Pause');
			psImg.anchor.setTo(0.5, 0.5);
			psImg.visible = false;

			emitter = game.add.emitter(0, 0, 200);
			emitter.makeParticles('pixel');
			emitter.setYSpeed(-speed, speed);
			emitter.setXSpeed(-speed, speed);
			emitter.setScale(8, 0, 8, 0, 800);
			emitter.gravity = 0;

			t_start = new Date();

			setInterval(function () {
				var rd = Math.random();
				var ax = (game.width - 200) * Math.random() + 100;
				if (rd > 0.75) {
					A = game.add.sprite(ax, 0, 'A');
					A.anchor.set(0.5, 0);
					A.scale.setTo(0.5, 0.5);
					game.physics.arcade.enable(A);
					A.outOfBoundsKill = true;
					A.checkWorldBounds = true;
					A.position.x = ax;
					A.body.velocity.y = speed;
				} else if (rd > 0.5) {
					H = game.add.sprite(ax, 0, 'H');
					H.anchor.set(0.5, 0);
					H.scale.setTo(0.5, 0.5);
					game.physics.arcade.enable(H);
					H.outOfBoundsKill = true;
					H.checkWorldBounds = true;
					H.position.x = ax;
					H.body.velocity.y = speed;
				} else if (rd > 0.25) {
					S = game.add.sprite(ax, 0, 'S');
					S.anchor.set(0.5, 0);
					S.scale.setTo(0.5, 0.5);
					game.physics.arcade.enable(S);
					S.outOfBoundsKill = true;
					S.checkWorldBounds = true;
					S.position.x = ax;
					S.body.velocity.y = speed;
				} else {
					B = game.add.sprite(ax, 0, 'B');
					B.anchor.set(0.5, 0);
					B.scale.setTo(0.5, 0.5);
					game.physics.arcade.enable(B);
					B.outOfBoundsKill = true;
					B.checkWorldBounds = true;
					B.position.x = ax;
					B.body.velocity.y = speed;
				}
			}, 6000);

		},
		update: function () {

			this.background.tilePosition.y += 2;
			this.movePlayer();
			if (fireButton.isDown) {
				pew_01.play();
				this.blt_02.fire();
			}
			if (superPower.downDuration(1)) {
				this.shootLaser();
			} else {
				laserbeam.position.x = player.position.x;
				laserbeam.position.y = player.position.y - player.height / 2;
			}
			//			if (E.downDuration(1)) {
			//				this.specialBlt();
			//			}

			baby.position.x = player.position.x;
			baby.position.y = player.position.y;
			baby.rotation -= 0.1;

			this.moveEnemy();
			this.changeVol();

			var sinx = 50 * Math.sin((this.game.time.totalElapsedSeconds() - temp_time) * 5);
			if (blt_04.children[0].alive) {
				if (i) sinx = -sinx;
				blt_04.children[0].position.x = sinx + temp_x;
				blt_04.children[0].body.velocity.y = -200;
				game.physics.arcade.overlap(blt_04.children[0], enemies, this.bulletCollision, null, this);
			}
			if (blt_04.children[1].alive) {
				blt_04.children[1].position.x = -sinx + temp_x;
				blt_04.children[1].body.velocity.y = -200;
				game.physics.arcade.overlap(blt_04.children[1], enemies, this.bulletCollision, null, this);
			}

			if (aimBlt_flag) {
				for (var i = 0; i < 5; i++) {
					if (enemies.getFirstAlive()) {
						aimBlt[i].rotation = game.physics.arcade.moveToObject(aimBlt[i], enemies.getFirstAlive(), 500, 300);
					} else {
						aimBlt[i].body.velocity.y = -300;
					}
				}
			}

			if (babyHP) {
				game.physics.arcade.overlap(baby, enemies, this.aimbulletCollision, null, this);
			}
			if (A) {
				game.physics.arcade.overlap(A, player, this.ACollision, null, this);
			}
			if (H) {
				game.physics.arcade.overlap(H, player, this.HCollision, null, this);
			}
			if (S) {
				game.physics.arcade.overlap(S, player, this.SCollision, null, this);
			}
			if (B) {
				game.physics.arcade.overlap(B, player, this.BCollision, null, this);
			}
			game.physics.arcade.overlap(blt_01, enemies, this.aimbulletCollision, null, this);
			game.physics.arcade.collide(enemies);
			game.physics.arcade.overlap(this.blt_02.bullets, enemies, this.bulletCollision, null, this);
			game.physics.arcade.overlap(laserbeam, enemies, this.laserCollision, null, this);
			for (var i = 0; i < e_num; i++) {
				game.physics.arcade.overlap(e_weapon[i].bullets, player, this.playerDie, null, this);
			}
			if (babyHP) {
				for (var i = 0; i < e_num; i++) {
					game.physics.arcade.overlap(baby, e_weapon[i].bullets, this.babybulletCollision, null, this);
				}
			}
		}, // No changes
		specialBlt: function () {

			if (!blt_04.children[0].alive && !blt_04.children[1].alive) {
				var x = temp_x = player.position.x;
				var y = player.position.y;
				for (var i = 0; i < 2; i++) {
					var temp = blt_04.getFirstExists(false);
					temp.reset(x, y);
					if (i) temp.tint = 0xffff00;
					temp.anchor.setTo(0.5, 0);
					temp.angle = -90;
					temp.visible = true;
					console.log(temp);
				}
				temp_time = this.game.time.totalElapsedSeconds();
			}

		},
		autoAiming: function () {
			if (aimBlt_flag == 0) {
				var x = player.position.x - 100;
				var y = player.position.y;

				for (var i = 0; i < 5; i++) {
					var temp = blt_01.getFirstExists(false);
					temp.anchor.setTo(0.5, 0.5);
					temp.angle = -90;
					temp.reset(x + 100, y);
					game.physics.arcade.moveToXY(temp, x + 50 * i, y, 1, 500);
					temp.visible = true;
					aimBlt.push(temp);
					console.log(temp);
				}
				setTimeout(function () {
					for (var i = 0; i < 5; i++) {
						aimBlt[i].body.velocity.x = 0;
						aimBlt[i].body.velocity.y = 0;
					}
					aimBlt_flag = 1;
				}, 500);
			}
		},
		movePlayer: function () {

			player.animations.play('flyIdle');
			player.body.velocity.x = 0;
			player.body.velocity.y = 0;

			if (rightKey.isDown) {
				player.animations.play('flyRight');
				player.body.velocity.x = speed;
			} else if (leftKey.isDown) {
				player.animations.play('flyLeft');
				player.body.velocity.x = -speed;
			}
			if (upKey.isDown) {
				player.animations.play('flyIdle');
				player.body.velocity.y = -speed;
			} else if (downKey.isDown) {
				player.animations.play('flyIdle');
				player.body.velocity.y = +speed;
			}

		}, // No changes
		addEnemy: function () {
			//			var enemy = enemies.getFirstDead();
			if (set) {
				var num = set.shift();
				if (num == 0) {
					var enemy = enemies.children[e_index];
					e_index++;
					if (!enemy) {
						return;
					}
					enemy.anchor.setTo(0.5, 0.5);
					enemy.reset(game.width / 2, 0);
					enemy.visibile = true;
					enemy.body.rotation = 0;
					enemy.body.gravity.y = 800;
					//					enemy.body.velocity.x = 1000;
					enemy.health = 20;
				} else {
					var rd = 100 * Math.sin(this.game.time.totalElapsedSeconds()) + game.width / 2;
					for (var i = 0; i < num; i++) {
						var enemy = enemies.children[e_index];
						e_index++;
						console.log(enemy);
						if (!enemy) {
							return;
						}
						var offset = (num - 1) / 2;
						var x = rd + (i - offset) * enemy.width * 1.5;
						enemy.anchor.setTo(0.5, 0.5);
						enemy.reset(x, 0);
						enemy.visibile = true;
						enemy.body.rotation = 0;
						//						enemy.body.gravity.y = 400;
						enemy.body.velocity.y = speed;
						//					enemy.body.velocity.x = 1000;
						enemy.health = 3;
						//					console.log(enemy);
					}
				}

			}

		},
		moveEnemy: function () {
			var i = 0;
			enemies.children.forEach(function (item) {

				dx = player.position.x - item.position.x;
				dy = player.position.y - item.position.y;

				if (item.key == 'boss') {
					if (dx * dx + dy * dy < 90000) {
						item.body.gravity.y = 0;
						item.body.velocity.y = -dy / 3;
						item.body.velocity.x = -dx / 6;
						if (item.alive) {
							e_weapon[i].fire();
						}
					} else if (dx * dx + dy * dy < 160000) {
						item.body.velocity.x = x / 4;
						if (item.alive) {
							e_weapon[i].fire();
						}
					} else {
						item.body.gravity.y = 0;
						item.body.velocity.y = dy / 3;
						item.body.velocity.x = dx / 6;
					}

				} else {
					if (item.position.y >= 180) {
						item.body.gravity.y = 0;
						item.body.velocity.y = 0;
						item.body.velocity.x = dx;
						if (item.alive) {
							e_weapon[i].fire();
						}
					}
				}

				item.rotation = game.physics.arcade.angleBetween(item, player);
				game.world.wrap(item, 0, true);
				i++;
				//				game.debug.spriteInfo(item, 32, 100);
				//				game.debug.spriteInfo(player, 32, 200);
			});

		}, // No changes
		shootLaser: function () {
			if (player.alive && power > 0) {
				//				console.log(2);
				power -= 1;
				//				powerLabel.text = "SP: " + power;
				SP[power].visible = false;
				laserbeam.position.x = player.position.x;
				laserbeam.position.y = player.position.y - player.height / 2;
				laserbeam.height = player.position.y;
				//				laserbeam.visible = true;
				laserbeam.animations.play('shoot');
				setTimeout(function () {
					laserbeam.height = 0;
					laserbeam.animations.stop(null, true);
				}, 2000);
			}
		},
		laserCollision: function (bullet, enemy) {

			enemy.health -= laserDamage;
			if (enemy.health <= 0) {
				this.enenmyDie(enemy);
				e_dead++;
				//				console.log(e_dead, e_index, e_num);
				if (e_dead - e_index == 0) {
					game.time.events.add(2000, function () {
						this.addEnemy();
					}, this);
				}
			}

		},
		HCollision: function () {
			babyHP = 3;
			baby.visible = true;
			H.kill();
		},
		ACollision: function () {
			this.autoAiming();
			A.kill();
		},
		BCollision: function () {
			this.specialBlt();
			B.kill();
		},
		SCollision: function () {
			power++;
			//			powerLabel.text = 'SP: ' + power;
			if (SP.length < power) {
				var temp_S = game.add.sprite(10 + SP.length * (30), 5 + 50, 'S');
				temp_S.anchor.set(0, 0);
				temp_S.scale.setTo(0.5, 0.5);
				SP.push(temp_S);
			} else {
				SP[power - 1].visible = true;
			}
			S.kill();
		},
		babybulletCollision: function (baby, bullet) {

			this.bulletDie(bullet);
			babyHP -= bulletDamage;
			console.log(babyHP, (babyHP <= 0), baby);
			if (babyHP <= 0) {
				baby.visible = false;
			}
			//			console.log(enemy.health);
		},
		aimbulletCollision: function (bullet, enemy) {

			this.bulletDie(bullet);
			enemy.health -= bulletDamage;
			if (enemy.health <= 0) {
				this.enenmyDie(enemy);
				e_dead++;
				//				console.log(e_dead, e_index, e_num);
				if (e_dead - e_index == 0) {
					game.time.events.add(2000, function () {
						this.addEnemy();
					}, this);
				}
			}
			aimBlt_flag = 0;
		},
		bulletCollision: function (bullet, enemy) {

			this.bulletDie(bullet);
			enemy.health -= bulletDamage;
			if (enemy.health <= 0) {
				this.enenmyDie(enemy);
				e_dead++;
				//				console.log(e_dead, e_index, e_num);
				if (e_dead - e_index == 0) {
					game.time.events.add(2000, function () {
						this.addEnemy();
					}, this);
				}
			}
			//			console.log(enemy.health);
		},
		changeVol: function () {
			if (addVol.downDuration(1)) {
				console.log(1);
				worldVol += 0.1;
				if (worldVol > 1.0) {
					worldVol = 1.0;
				}
			} else if (subVol.downDuration(1)) {
				worldVol -= 0.1;
				if (worldVol < 0.0) {
					worldVol = 0.0;
				}
			}
			worldVol = Math.round(worldVol * 100) / 100;
			bgm.volume = worldVol;
			pew_01.volume = worldVol;
			//			volumeLabel.text = "Volume: " + worldVol;
			volume.width *= bgm.volume;
			volume.scale.setTo(bgm.volume, bgm.volume);
		},
		stop: function () {
			pause = !pause;
			if (pause) {
				psImg.visible = true;
				game.paused = true;
			} else {
				game.paused = false;
				psImg.visible = false;
			}
		},
		bulletDie: function (bullet) {
			emitter.setAll('tint', 0x66ffff);
			emitter.x = bullet.x;
			emitter.y = bullet.y;
			bullet.kill();
			emitter.start(true, 800, null, 8);
		},
		ebulletDie: function (bullet) {
			emitter.setAll('tint', 0xff0000);
			emitter.x = bullet.x;
			emitter.y = bullet.y;
			bullet.kill();
			emitter.start(true, 800, null, 8);
		},
		enenmyDie: function (enemy) {
			emitter.setAll('tint', 0xffffff);
			emitter.x = enemy.x;
			emitter.y = enemy.y;
			enemy.kill();
			emitter.start(true, 1000, null, 15);
			game.global.score += 300;
			scoreLabel.text = 'score: ' + game.global.score;
			if (enemy.key === "boss") {
				sendUserData(1);
				game.time.events.add(2500, function () {
					game.state.start('end');
				}, this);
			}
		},
		playerDie: function (player, bullet) {
			this.ebulletDie(bullet);
			health--;
			HP[health].visible = false;
			if (health <= 0) {
				//				healthLabel.text = "HP: " + health;
				emitter.setAll('tint', 0xffffff);
				emitter.x = player.x;
				emitter.y = player.y;
				player.kill();
				bgm.destroy();
				game.input.keyboard.stop();
				game.camera.flash(0xffffff, 300);
				game.camera.shake(0.02, 300);
				emitter.start(true, 1000, null, 15);
				sendUserData(0);
				game.time.events.add(2500, function () {
					game.input.keyboard.start();
					game.state.start('end');
				}, this);
			}
		},
		render: function () {
			//			game.debug.soundInfo(bgm, 20, 32);
			//			game.debug.text('Living baddies: ' + (enemies.countLiving()), game.width / 2, game.height);

		}
	};

	function sendUserData(win) {
		var bonus = 0;
		if (win) {
			t_end = new Date();
			var diff = t_end.getSeconds() - t_start.getSeconds();
			bonus = 2000;
		}
		game.global.score += bonus;
		var total = game.global.score;
		firebase.database().ref('users/').push().set({
			score: total,
			name: name
		});
	}


