var bgm;
var button;
var button2;
var card;
var btnCancel;
var btnSignUp;
var name;
var ctrl;

var menuState = {
	create: function () {
		// Add a background image
		game.add.image(0, 0, 'cover');

		// Display the name of the game
		var nameLabel = game.add.text(game.width / 2, game.height * 2 / 5, 'Raiden', {
			font: '130px Arial',
			fill: '#ffffff'
		});
		nameLabel.anchor.setTo(0.5, 0.5);

		bgm = game.add.audio('bgm');
		bgm.loop = true;
		bgm.volume = 0.5;
		bgm.play();

		button = game.add.button(game.world.centerX, game.height * 3 / 5, 'button', this.actionOnClick, this, 1, 0, 2);
		button.anchor.setTo(0.5, 0.5);
		button2 = game.add.button(game.world.centerX, game.height * 3 / 5 + 70, 'button2', function () {
			ctrl.visible = true;
		}, this, 1, 0, 2);
		button2.anchor.setTo(0.5, 0.5);

		card = document.getElementById('card');
		btnSignUp = document.getElementById('btnSignUp');
		btnCancel = document.getElementById('btnCancel');

		btnCancel.addEventListener('click', function () {
			document.getElementById('overlay').style.display = 'none';
			var n = card.className.split(' ');
			n[n.length - 1] = 'scale-out-center';
			card.className = n.join(' ');
			setTimeout(function () {
				card.style.display = 'none';
			}, 600);

		});

		btnSignUp.addEventListener('click', async function () {
			name = document.getElementById('name').value;
//			name = securitytxt(name);
			if (name === '') {
				document.getElementById('alert').innerHTML = 'Your name cannot be blank.';
				return;
			} else {
				document.getElementById('alert').innerHTML = 'Please enter your name.';
				document.getElementById('overlay').style.display = 'none';
				var n = card.className.split(' ');
				n[n.length - 1] = 'scale-out-center';
				card.className = n.join(' ');
				setTimeout(function () {
					card.style.display = 'none';
					game.state.start('play');
				}, 600);
			}

		});

		ctrl = game.add.image(game.world.centerX, game.world.centerY, 'ctrl');
		ctrl.anchor.setTo(0.5, 0.5);
		ctrl.visible = false;

		var esc = game.input.keyboard.addKey(Phaser.KeyCode.ESC);
		esc.onDown.add(function () {
			ctrl.visible = false;
		}, this);

	},
	actionOnClick: function () {
		document.getElementById('canvas').style.zIndex = -1;
		document.getElementById('overlay').style.display = 'block';
		card.style.display = 'block';
		var n = card.className.split(' ');
		n[n.length - 1] = 'scale-up-center';
		card.className = n.join(' ');

	}
};

function securitytxt(text) {
	var securitytxt = text.replace('&', '&amp;');
	securitytxt = securitytxt.replace('>', '&gt;');
	securitytxt = securitytxt.replace('<', '&lt;');
	securitytxt = securitytxt.replace('"', '&quot;');
	securitytxt = securitytxt.replace("'", '&#x27;');
	securitytxt = securitytxt.replace("/", '&#x2F;');
	return securitytxt
}
