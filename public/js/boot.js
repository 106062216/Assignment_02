var bootState = {
	preload: function () {
		// Load the progress bar image.
		game.load.image('progressBar', 'assets/img/progressBar.png');
	},
	create: function () {
		// Set some game settings.
		game.stage.backgroundColor = '#000000';
		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.renderer.renderSession.roundPixels = true;
		// Start the load state.
		game.state.start('load');
	}
};

